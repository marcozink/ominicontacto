# Release Notes
2024-07-12

## Added

- oml-2700 Agents to transfer ordered by Ready state
- oml-2674 Whatsapp conversations can be exported to .csv file

## Changed

- oml-2701 Event Audit allows filtering by day
- oml-2703 New Outbound Routes regex for special characters
- oml-2708 Several reports button blocked on click to prevent server overload
- oml-2162 Tests optimization

## Fixed

- oml-2669 Fix inconsistent agents activity logging
- oml-2569 Fix previous day data showing in Inbound calls Supervision
- Fix External Site Json Authentication

## Removed

No removals in this release.
